package tgpr.tuto.controller;

import tgpr.tuto.TutoApp;
import tgpr.tuto.view.MainMenuView;
import tgpr.tuto.view.View;

public class MainMenuController extends Controller {

    public void run() {
        MainMenuView view = new MainMenuView();
        try {
            View.Action res;
            do {
                view.displayHeader();
                view.displayMenu();
                res = view.askForAction();
                switch (res.getAction()) {
                    case 'L':
                        new MemberListController().run();
                        break;
                }
            } while (res.getAction() != 'O');
        } catch (View.ActionInterruptedException e) {
            view.pausedWarning("logged out");
        }

        TutoApp.logout();
    }
}