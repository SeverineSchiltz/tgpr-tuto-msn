package tgpr.tuto.controller;

import tgpr.tuto.TutoApp;
import tgpr.tuto.model.Member;
import tgpr.tuto.view.ProfileUpdateView;
import tgpr.tuto.view.View;

import java.time.LocalDate;
import java.util.List;

public class ProfileUpdateController extends Controller {
    private final Member member;
    private final ProfileUpdateView view = new ProfileUpdateView();

    public ProfileUpdateController(Member member) {
        this.member = member;
    }

    public void run() {
        //view.displayHeader();
        View.Action res;
        List<String> errors;
        try {
            Member current = TutoApp.getLoggedUser();
//            view.displayPseudo(member.getPseudo());
//            String profile = view.askProfile(member.getProfile());
//            boolean admin = view.askAdmin(member.isAdmin());
//            //LocalDate birthDate = view.askBirthDate(member.getBirthdate());
//            LocalDate birthDate = askBirthDate(member.getBirthdate());
//            res = view.askForAction();
//            if (res.getAction() == 'O') {
            do {
                // encodage des données
                view.displayPseudo(member.getPseudo());
                String profile = view.askProfile(member.getProfile());
                //boolean admin = view.askAdmin(member.isAdmin());
                boolean admin = member.isAdmin();
                // on peut changer le rôle si on est admin et qu'on ne se modifie pas soi-même
                if (current.isAdmin() && !member.equals(current))
                    admin = view.askAdmin(admin);
                else
                    view.displayAdmin(member.isAdmin());
                LocalDate birthDate = askBirthDate(member.getBirthdate());

                // validations métier
                member.setProfile(profile);
                member.setAdmin(admin);
                member.setBirthdate(birthDate);
//                member.save();
//            }
                errors = member.validate();
                // affichage des erreurs
                if (errors.size() > 0)
                    view.showErrors(errors);

            } while (errors.size() > 0); // on recommence tant qu'il y a des erreurs

            res = view.askForAction();
            if (res.getAction() == 'O')
                member.save();   // sauvegarde des nouvelles données
            else
                member.reload();
        } catch (View.ActionInterruptedException e) {
            member.reload();
            view.pausedWarning("update profile aborted");
        }
    }

    public LocalDate askBirthDate(LocalDate actual) {
        LocalDate date;
        String error;
        do {
            date = view.askBirthDate(actual);
            error = Member.isValidBirthdate(date);
            if (error != null) view.error(error);
        } while (error != null);
        return date;
    }

}