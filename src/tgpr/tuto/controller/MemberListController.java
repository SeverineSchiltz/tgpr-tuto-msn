package tgpr.tuto.controller;

import tgpr.tuto.TutoApp;
import tgpr.tuto.model.Member;
import tgpr.tuto.view.MemberListView;
import tgpr.tuto.view.View;

public class MemberListController extends Controller {
    @Override
    public void run() {
        var members = Member.getAll();
//        for (var m : members)
//            System.out.println(m);
        var view = new MemberListView();
//        view.displayHeader();
//        view.displayMembers(members);
//        view.pause();
        try {
            View.Action res;
            do {
                view.displayHeader();
                view.displayMembers(members);
                var current = TutoApp.getLoggedUser();
                res = view.askForAction(members.size());
                switch (res.getAction()) {
                    case 'V':
                        var m = members.get(res.getNumber() - 1);
                        //view.pausedWarning(String.format("You selected '%s'", m.getPseudo()));
                        new ProfileController(m).run();
                        break;
                    case 'F':
                        current.follow(members.get(res.getNumber() - 1));
                        break;
                    case 'U':
                        current.unfollow(members.get(res.getNumber() - 1));
                        break;
                }
            } while (res.getAction() != 'L');
        } catch (View.ActionInterruptedException e) {
            // just leave the loop
        }
//        view.pausedWarning("Leaving the application");
//        view.close();
    }
}