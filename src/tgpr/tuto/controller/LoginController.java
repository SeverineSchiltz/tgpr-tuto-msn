package tgpr.tuto.controller;

import tgpr.tuto.TutoApp;
import tgpr.tuto.model.Member;
import tgpr.tuto.view.LoginView;
import tgpr.tuto.view.View;

public class LoginController extends Controller {

    private final LoginView view = new LoginView();

    private Member askPseudo() {
        String pseudo;
        Member member;
        do {
            pseudo = view.askPseudo();
            member = Member.getByPseudo(pseudo);
            if (member == null)
                view.error("unknown user");
        } while (member == null);
        return member;
    }

    private String askPassword(Member member) {
        String password = null;
        do {
            password = view.askPassword();
            if (!password.equals(member.getPassword()))
                view.error("bad password");
        } while (!password.equals(member.getPassword()));
        return password;
    }

    public void run() {
        view.displayHeader();
        try {
            var member = askPseudo();
            askPassword(member);
            TutoApp.setLoggedUser(member);
            new MainMenuController().run();
        } catch (View.ActionInterruptedException e) {
            view.pausedWarning("aborted login");
        }
    }
}