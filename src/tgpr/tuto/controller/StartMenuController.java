package tgpr.tuto.controller;

import tgpr.tuto.view.StartMenuView;
import tgpr.tuto.view.View;

public class StartMenuController extends Controller {
    public void run() {
        StartMenuView view = new StartMenuView();
        try {
            View.Action res;
            do {
                view.displayHeader();
                view.displayMenu();
                res = view.askForAction();
                switch (res.getAction()) {
                    case 'L':
                        new LoginController().run();
                        break;
                }
            } while (res.getAction() != 'Q');
        } catch (View.ActionInterruptedException e) {
            // just leave
        }
        view.pausedWarning("leaving the application");
        view.close();
    }
}