package tgpr.tuto.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Member extends Model {
//    protected static Connection db;
//
//    static {
//        try {
//            db = DriverManager.getConnection("jdbc:mariadb://localhost:3306/tgpr_msn?user=root&password=root");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public String toString() {
        return "model.Member{" +
                "pseudo='" + pseudo + '\'' +
                ", profile='" + profile + '\'' +
                ", birthdate=" + birthdate +
                ", admin=" + admin +
                '}';
    }

    public Member() {
    }

    public Member(String pseudo, String password, boolean admin) {
        this.pseudo = pseudo;
        this.password = password;
        this.admin = admin;
    }

    public Member(String pseudo, String password, String profile, boolean admin, LocalDate birthdate) {
        this.pseudo = pseudo;
        this.password = password;
        this.profile = profile;
        this.admin = admin;
        this.birthdate = birthdate;
    }

    private String password;
    private String profile;
    private boolean admin;
    private LocalDate birthdate;

    private String pseudo;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public static String isValidBirthdate(LocalDate birthdate) {
        if (birthdate == null)
            return null;
        if (birthdate.compareTo(LocalDate.now()) > 0)
            return "may not be born in the future";
        if (Period.between(birthdate, LocalDate.now()).getYears() < 18)
            return "must be 18 years old";
        return null;
    }

    public Member(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }


    public List<String> validate() {
        var errors = new ArrayList<String>();

        // field validations
        var err = isValidBirthdate(birthdate);
        if (err != null) errors.add(err);

        // cross-fields validations
        if (profile != null && profile.equals(pseudo))
            errors.add("profile and pseudo must be different");

        return errors;
    }

    public void reload() {
        try {
            var stmt = Model.db.prepareStatement("select * from members where pseudo=?");
            stmt.setString(1, pseudo);
            var rs = stmt.executeQuery();
            if (rs.next()) {
                mapper(rs, this);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void mapper(ResultSet rs, Member member) throws SQLException {
        member.pseudo = rs.getString("pseudo");
        member.password = rs.getString("password");
        member.profile = rs.getString("profile");
        member.admin = rs.getBoolean("admin");
        member.birthdate = rs.getObject("birthdate", LocalDate.class);
    }

    public static List<Member> getAll() {
        var list = new ArrayList<Member>();
        try {
            var stmt = db.prepareStatement("select * from members order by pseudo");
            var rs = stmt.executeQuery();
            while (rs.next()) {
                var member = new Member();
                mapper(rs, member);
                list.add(member);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Member getByPseudo(String pseudo) {
        Member member = null;
        try {
            var stmt = db.prepareStatement("select * from members where pseudo=?");
            stmt.setString(1, pseudo);
            var rs = stmt.executeQuery();
            if (rs.next()) {
                member = new Member();
                mapper(rs, member);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return member;
    }

    public boolean save() {
        Member m = getByPseudo(pseudo);
        int count = 0;
        try {
            PreparedStatement stmt;
            if (m == null) {
                stmt = db.prepareStatement("insert into Members (pseudo, password, profile, admin, birthdate) values (?,?,?,?,?)");
                stmt.setString(1, pseudo);
                stmt.setString(2, password);
                stmt.setString(3, profile);
                stmt.setBoolean(4, admin);
                stmt.setObject(5, birthdate);
            } else {
                stmt = db.prepareStatement("update Members set password=?, profile=?, admin=?, birthdate=? where pseudo=?");
                stmt.setString(1, password);
                stmt.setString(2, profile);
                stmt.setBoolean(3, admin);
                stmt.setObject(4, birthdate);
                stmt.setString(5, pseudo);
            }
            count = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return count == 1;
    }

    public boolean delete() {
        int count = 0;
        try {
            PreparedStatement stmt = db.prepareStatement("delete from Members where pseudo=?");
            stmt.setString(1, pseudo);
            count = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return count == 1;
    }

    @Override
    public boolean equals(Object o) {
        // si les deux références sont identiques, il s'agit du même objet et ils sont donc égaux
        if (this == o) return true;
        // faux si l'objet o est nul ou si l'objet courant et l'objet o n'ont pas le même type (la même classe)
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        // si les objets ont le même pseudo, ils sont égaux
        return pseudo.equals(member.pseudo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pseudo);
    }

    public List<Member> getFollowers() {
        var list = new ArrayList<Member>();
        try {
            var stmt = Model.db.prepareStatement("select m.* from Follows join Members m on Follows.follower = m.pseudo where followee=?");
            stmt.setString(1, pseudo);
            var rs = stmt.executeQuery();
            while (rs.next()) {
                var member = new Member();
                mapper(rs, member);
                list.add(member);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Member> getFollowees() {
        var list = new ArrayList<Member>();
        try {
            var stmt = Model.db.prepareStatement("select m.* from Follows join Members m on Follows.followee = m.pseudo where follower=?");
            stmt.setString(1, pseudo);
            var rs = stmt.executeQuery();
            while (rs.next()) {
                var member = new Member();
                mapper(rs, member);
                list.add(member);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public boolean follow(Member other) {
        if (other.equals(this))
            return false;
        if (!getFollowees().contains(other)) {
            int count = 0;
            try {
                PreparedStatement stmt = Model.db.prepareStatement("insert into Follows values (?,?)");
                stmt.setString(1, pseudo);
                stmt.setString(2, other.pseudo);
                count = stmt.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return count == 1;
        }
        return false;
    }

    public boolean unfollow(Member other) {
        if (other.equals(this))
            return false;
        int count = 0;
        try {
            PreparedStatement stmt = Model.db.prepareStatement("delete from Follows where follower=? and followee=?");
            stmt.setString(1, pseudo);
            stmt.setString(2, other.pseudo);
            count = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return count == 1;
    }

    public boolean isFollower(Member other) {
        return getFollowees().contains(other);
    }

    public boolean isFollowee(Member other) {
        return getFollowers().contains(other);
    }

    public boolean isMutualFriend(Member other) {
        return isFollower(other) && isFollowee(other);
    }

}