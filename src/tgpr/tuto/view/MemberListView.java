package tgpr.tuto.view;

import tgpr.tuto.model.Member;
import tgpr.tuto.TutoApp;
import java.util.List;

public class MemberListView extends View {

    public void displayHeader() {
//        clear();
//        println("\n=== Member List ===\n");
        displayHeader("Member List");
    }

    private void displayOneMember(int i, Member m) {
        String status = "";
        if (m.isMutualFriend(TutoApp.getLoggedUser()))
            status = "(mutual friend)";
        else if (m.isFollower(TutoApp.getLoggedUser()))
            status = "(following you)";
        else if (m.isFollowee(TutoApp.getLoggedUser()))
            status = "(followed by you)";
        printf("[%2d] %s %s\n", i, m.getPseudo(), status);
    }

    public void displayMembers(List<Member> members) {
//        for (var m : members)
//            //System.out.println(m);
//            println(m);
        int i = 1;
        for (var m : members) {
            //printf("[%2d] %s\n", i, m.getPseudo());
            displayOneMember(i, m);
            ++i;
        }
    }

//    public View.Action askForAction(int size) {
//        return doAskForAction(size, "\n[V] View, [L] Leave",
//                "[vV][0-9]+|[lL]");
//    }
    public View.Action askForAction(int size) {
        return doAskForAction(size, "\n[F] Follow, [U] Unfollow, [V] View, [L] Leave",
                "[vV][0-9]+|[fF][0-9]+|[uU][0-9]+|[lL]");
    }
}