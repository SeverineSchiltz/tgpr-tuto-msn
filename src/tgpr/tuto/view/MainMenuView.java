package tgpr.tuto.view;

public class MainMenuView extends View {
    public void displayHeader() {
//        clear();
//        println("\n=== Main Menu ===\n");
        displayHeader("Main Menu");
    }

    public void displayMenu() {
        println("[L] List of members");
        println("\n[O] Logout");
    }

    public Action askForAction() {
        return doAskForAction(-1, "", "[oO]|[lL]");
    }
}