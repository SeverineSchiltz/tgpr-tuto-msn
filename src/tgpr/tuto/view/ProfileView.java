package tgpr.tuto.view;

import tgpr.tuto.model.Member;

import java.time.format.DateTimeFormatter;

public class ProfileView extends View {
    private final Member member;

    public ProfileView(Member member) {
        this.member = member;
    }

    public void displayHeader() {
//        clear();
//        println("\n=== " + member.getPseudo() + "'s Profile ===\n");
        displayHeader(member.getPseudo() + "'s Profile");
    }

    public void displayProfile() {
        println("Pseudo: " + member.getPseudo());
        println("Profile: " + member.getProfile());
        println("Admin: " + member.isAdmin());
        println("Birth Date: " +
                (member.getBirthdate() == null ?
                        "null" :
                        DateTimeFormatter.ofPattern("dd/MM/yyyy").format(member.getBirthdate())));
    }

    public View.Action askForAction() {
        //return doAskForAction(-1, "\n[L] Leave", "[lL]");
        return doAskForAction(-1, "\n[L] Leave", "[lL]");
    }

    public View.Action askForActionAdmin() {
        return doAskForAction(-1, "\n[U] Update, [L] Leave", "[uU]|[lL]");
    }
}