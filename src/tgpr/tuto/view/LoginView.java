package tgpr.tuto.view;

public class LoginView extends View {

    public void displayHeader() {
        clear();
        println("\n=== Login ===\n");
    }

    public String askPseudo() {
        return askString("Pseudo: ", null);
    }

    public String askPassword() {
        return askString("Password: ", null, true);
    }
}