package tgpr.tuto.view;

public class StartMenuView extends View {
    public void displayHeader() {
        clear();
        println("\n=== Start Menu ===\n");
    }

    public void displayMenu() {
        println("[L] Login");
        println("\n[Q] Quit");
    }

    public Action askForAction() {
        return doAskForAction(-1, "", "[qQ]|[lL]");
    }
}