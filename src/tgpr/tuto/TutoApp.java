package tgpr.tuto;

import tgpr.tuto.controller.MemberListController;
import tgpr.tuto.controller.StartMenuController;
import tgpr.tuto.model.Member;
import tgpr.tuto.model.Model;
import tgpr.tuto.view.ErrorView;

public class TutoApp {

    private static Member loggedUser = null;

    public static Member getLoggedUser() {
        return loggedUser;
    }

    public static void setLoggedUser(Member loggedUser) {
        TutoApp.loggedUser = loggedUser;
    }

    public static boolean isLogged() {
        return loggedUser != null;
    }

    public static void logout() {
        setLoggedUser(null);
    }

    public static boolean isAdmin() {
        return loggedUser != null && loggedUser.isAdmin();
    }

    public static void main(String[] args) {
        //new MemberListController().run();
        if (!Model.checkDb())
            new ErrorView("Database is not available").close();
        else
            //new MemberListController().run();
            new StartMenuController().run();
    }

//    private void testModel() {
//        //System.out.println("Hello World!");
//        //var ben = new model.Member("ben");
//        //System.out.println(ben);
//
////        var members = model.Member.getAll();
////        for (var m : members)
////            System.out.println(m);
////        System.out.println("------------------");
////        var ben = model.Member.getByPseudo("ben");
////        System.out.println(ben);
//
//        System.out.println("\nListe des membres :");
//        var members = Member.getAll();
//        for (var m : members)
//            System.out.println(m);
//
//        System.out.println("\nMembre 'ben' :");
//        var ben = Member.getByPseudo("ben");
//        System.out.println(ben);
//
//        System.out.println("\nNouveau membre 'test'");
//        var test = new Member("test", "test", false);
//        // sauvergarde
//        boolean res = test.save();
//        assert res;
//        // relecture en BD
//        test = Member.getByPseudo("test");
//        assert test != null;
//        System.out.println(test);
//        // suppression
//        res = test.delete();
//        assert res;
//        assert Member.getByPseudo("test") == null;
//    }
}
